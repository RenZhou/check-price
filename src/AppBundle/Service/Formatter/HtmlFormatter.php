<?php


namespace AppBundle\Service\Formatter;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;

class HtmlFormatter
{
    /**
     * @param $data
     * @return JsonResponse
     */
    public function formatToJson(ArrayCollection $data)
    {
        return new JsonResponse(
            array(
                "product" => $data->getValues()
            ),
            200,
            array()
        );
    }
}