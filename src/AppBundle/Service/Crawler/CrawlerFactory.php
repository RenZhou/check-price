<?php


namespace AppBundle\Service\Crawler;

use AppBundle\Service\Crawler\Product\ProductCrawlerFactory;
use Symfony\Component\Config\Definition\Exception\Exception;

class CrawlerFactory implements ICrawlerFactory
{

    /**
     * Function to create a factory with the right type and hostname
     *
     * @param $type
     * @param $hostname
     * @return mixed
     */
    public function makeCrawler($type, $hostname)
    {
        $factory = null;
        switch ($type) {
            case "product":
                $factory = new ProductCrawlerFactory();
                break;
            default:
                throw new Exception("This Factory doesn't exist");
        }
        return $factory->makeCrawler($type, $hostname);
    }
}