<?php


namespace AppBundle\Service\Crawler;


use Doctrine\Common\Collections\ArrayCollection;

interface ICrawler
{
    /**
     * This function should process the data maybe html code, anywhere :D
     *
     * @return mixed
     */
    public function process();

    /**
     * This function should check data is uniforme
     *
     * @param ArrayCollection $data
     * @return
     */
    public function check(ArrayCollection $data);

    /**
     *This function return the data processed
     *
     * @return mixed
     */
    public function getData();
}