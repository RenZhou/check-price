<?php


namespace AppBundle\Service\Crawler;


interface ICrawlerFactory
{
    /**
     * Function to create a factory with the right type and hostname
     *
     * @param $type
     * @param $hostname
     * @return mixed
     */
    public function makeCrawler($type, $hostname);
}