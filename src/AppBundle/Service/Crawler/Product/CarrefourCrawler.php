<?php


namespace AppBundle\Service\Crawler\Product;


use Doctrine\Common\Collections\ArrayCollection;

class CarrefourCrawler extends ProductCrawler
{
    /**
     * This function should return the data commun for all crawler
     *
     * @return mixed
     */
    public function process()
    {
        $this->data = new ArrayCollection();
        $this->data->add(
            array(
                "name" => "hi"
            )
        );
        $this->data->add(
            array(
                "name" => "hello"
            )
        );
    }

}