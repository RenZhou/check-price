<?php


namespace AppBundle\Service\Crawler\Product;


use AppBundle\Service\Crawler\ICrawler;
use Doctrine\Common\Collections\ArrayCollection;

abstract class ProductCrawler implements ICrawler
{
    protected $data;

    public function check(ArrayCollection $data)
    {
        // TODO: Implement check() method.
        return true;
    }

    public function getData()
    {
        $this->process();

        if ($this->check($this->data)) {
            return $this->data;
        }
        return null;
    }
}