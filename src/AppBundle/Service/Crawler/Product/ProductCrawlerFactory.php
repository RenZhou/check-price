<?php

namespace AppBundle\Service\Crawler\Product;


use AppBundle\Service\Crawler\ICrawlerFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Config\Definition\Exception\Exception;

class ProductCrawlerFactory implements ICrawlerFactory
{
    /**
     * Function to create a factory with the right type and hostname
     *
     * @param $type
     * @param $hostname
     * @return mixed
     */
    public function makeCrawler($type, $hostname)
    {
        switch ($hostname) {
            case "carrefour":
                return new CarrefourCrawler();
            default:
                throw new Exception("This factory doesn't exist");
        }
    }
}