<?php

namespace AppBundle\Controller;

use AppBundle\Service\Crawler\ICrawler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api/v1/crawler", name="crawler-homepage")
 */
class ProductCrawlerController extends Controller
{
    private $type = "product";

    /**
     * @Route("/get_all", name="all_data")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $hostname = "carrefour";

        $factory = $this->get("crawler_factory");
        $formatter = $this->get("formatter");

        /** @var ICrawler $crawler */
        $crawler = $factory->makeCrawler($this->type, $hostname);
        $data = $crawler->getData();

        return $formatter->formatToJson($data);
    }

}
