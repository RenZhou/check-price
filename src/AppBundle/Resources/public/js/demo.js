/**
 * Created by ren on 01/04/2016.
 */

function drawSquare() {
    var canvas = document.getElementById('content');
    if (canvas.getContext('2d')) {
        var context = canvas.getContext('2d');

        context.fillStyle = "rgb(13, 118, 208)";
        context.fillRect(2, 2, 798, 798);
    } else {
        alert("This page has not convas");
    }
}

function rangeCheck() {
    var i = document.createElement('input');
    i.setAttribute('type', 'range');

    if (i.type == 'text') {
        document.write('not');
    } else {
        document.write('yes');
    }
}

var canvas = document.getElementById('game');
var context = canvas.getContext('2d');
var radius = 20;
var locor = "#0000ff";
var g = 0.1;
var x = 50;
var y = 50;
var vx = 2;
var vy = 0;

window.onload = init;

function init() {
    setInterval(onEachStep, 1000 / 6);
}

function onEachStep() {
    vy += g;
    x += vx;
    y += vy;
    if (y > canvas.height - radius) {
        y = canvas.height - radius;
        vy *= -0.8;
    }
    if (x > canvas.width + radius) {
        x = -radius;
    }
    drawBall();
}

function drawBall() {
    with (context) {
        clearRect(0, 0, canvas.width, canvas.height);
        fillStyle = color;
        beginPath();
        arc(x, y, radius, 0, 2 * Math.PI, true);
        closePath();
        fill();
    };
}